<?php

namespace VIP\Models\Service\Push;

use Bitrix\Main\Type\DateTime;
use VIP\Models\User\PushToken;
use VIP\Vendor\Db\HighloadIblockElementModel;
use VIP\Vendor\Support\Status;


/**
 * Class Push
 *
 * @package VIP\Models\Catalog
 * @property $id;
 * @property $type;
 * @property DateTime $date;
 * @property $status;
 * @property $event;
 * @property $tokenId;
 * @property $errorReason;
 * @property $additionalParams;
 * @property $pushTaskId;
 * @property DateTime $dispatchTime;
 * @property $isOpen;
 * @property DateTime $dateOpen;
 */
class PushSubTasks extends HighloadIblockElementModel
{
    const TYPE_PUSH_MOBILE = 165;
    const TYPE_PUSH_DESKTOP = 166;

    const STATUS_COMPLETE = 167;
    const STATUS_AWAIT = 168;
    const STATUS_CANCEL = 169;
    const STATUS_ERROR = 170;
    const STATUS_IN_DISPATCH = 171;

    public static function fields(): array
    {
        return [
            'id' => 'ID',
            'type' => 'UF_TYPE',
            'date' => 'UF_DATE',
            'status' => 'UF_STATUS',
            'tokenId' => 'UF_TOKEN_ID',
            'errorReason' => 'UF_ERROR_REASON',
            'additionalParams' => 'UF_ADDITIONAL_PARAMS',
            'dispatchTime' => 'UF_DISPATCH_TIME',
            'pushTaskId' => 'UF_PUSH_TASK_ID',
            'isOpen' => 'UF_IS_OPEN',
            'dateOpen' => 'UF_DATE_OPEN',
        ];
    }

    public function __construct()
    {
        $this->date = new DateTime();
        $this->status = static::STATUS_AWAIT;
        $this->isOpen = 0;
        parent::__construct();
    }

    static function iblockId()
    {
        return 19;
    }

    public function cancel($reason)
    {
        $this->errorReason = $reason;
        $this->status = self::STATUS_CANCEL;
    }

    public function fail($reason)
    {
        $this->errorReason = $reason;
        $this->status = self::STATUS_ERROR;
    }

    public function complete()
    {
        $this->status = self::STATUS_COMPLETE;
    }

    public function dispatch()
    {
        $this->status = self::STATUS_IN_DISPATCH;
    }

    public function error($message)
    {
        $this->statusId = static::STATUS_ERROR;
        $this->message = $message;
    }

    public static function findAllAwait()
    {
        return static::findAll([
            'status' => static::STATUS_AWAIT,
            '<=dispatchTime' => new DateTime(),
        ]);
    }

    public function execute(): Status
    {
        $token = PushToken::load($this->tokenId);
        if ($token) {
            $params = json_decode($this->additionalParams, true);
            $params['data']['subTaskId'] = $this->id;
            $token->send($params);
            return new Status(Status::SUCCESS);
        } else {
            return new Status(Status::ERROR, 'Токены не найдены');
        }
    }
}