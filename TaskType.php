<?php

namespace app\modules\push\models\pushTasks\taskTypes;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string|null $name
 */
class TaskType extends ActiveRecord
{
    const OPEN_APPLICATION = 1;
    const OPEN_NEW = 2;
    const OPEN_SALE = 3;
    const OPEN_CATALOG = 4;
    const OPEN_DIRECTORY_SECTION = 5;
    const OPEN_PRODUCT = 6;
    const OPEN_COLLECTION = 7;
    const OPEN_BRAND_PAGE = 8;

    public bool $isShowGender = false;
    public bool $isShowSection = false;
    public bool $isShowProductId = false;
    public bool $isShowProductName = false;
    public bool $isShowCollection = false;
    public bool $isShowBrand = false;

    public static function tableName(): string
    {
        return 'push_tasks_types';
    }

    public static function instantiate($row): TaskType
    {
        switch ($row['id']) {
            case TaskType::OPEN_APPLICATION:
                return new OpenApplication();
            case TaskType::OPEN_NEW:
                return new OpenNew();
            case TaskType::OPEN_SALE:
                return new OpenSale();
            case TaskType::OPEN_CATALOG:
                return new OpenCatalog();
            case TaskType::OPEN_DIRECTORY_SECTION:
                return new OpenDirectorySection();
            case TaskType::OPEN_PRODUCT:
                return new OpenProduct();
            case TaskType::OPEN_COLLECTION:
                return new OpenCollection();
            case TaskType::OPEN_BRAND_PAGE:
                return new OpenBrandPage();
            default:
                return new static();
        }
    }

    public function rules(): array
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}