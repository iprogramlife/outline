<?php

namespace VIP\Controllers;

use VIP\Models\Feedback\Questionary;
use VIP\Models\Feedback\Answer;
use VIP\Vendor\Base\Application;
use VIP\Vendor\Context\Request;
use VIP\Seo\Fbk as Seo;
use VIP\Vendor\Controller\Web;

class Fbk extends Web
{
    public function detail()
    {
        $id = Request::get('id');
        if (!$id) {
            return $this->render404();
        }
        $questionary = Questionary::load($id);
        Application::setSeo(new Seo\Detail($questionary));

        Application::setAdditionalBodyClass('feedbackpage');
        if (!$questionary) {
            return $this->render404();
        }

        return $this->render('vueapp');
    }

    public function success()
    {
        $id = Request::get('id');
        if (!$id) {
            return $this->render404();
        }
        $answer = Answer::load($id);
        if (!$answer) {
            return $this->render404();
        }
        Application::setSeo(new Seo\Success());

        return $this->render('vueapp');
    }
}