<?php

namespace app\modules\push\models\pushTasks;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use app\modules\push\models\pushTasks\taskTypes\TaskType;
use app\modules\push\models\pushTasks\taskAudiences\TaskAudience;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property string $dateDelayed
 * @property string $dateCreate
 * @property int $pushTasksStatusesId
 * @property int $pushTasksAudiencesId
 * @property int $pushTasksTypesId
 * @property TaskAudience $pushTasksAudience
 * @property TaskStatus $pushTasksStatus
 * @property TaskType $pushTasksType
 * @property int $gender
 * @property int $sectionId
 * @property int $collectionId
 * @property int $brandId
 * @property int $productId
 * @property string $productName
 * @property int $usersId
 */
class Task extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'push_tasks';
    }

    public function rules(): array
    {
        return [
            [['text', 'title'], 'string'],
            [['text', 'title', 'pushTasksAudiencesId', 'dateDelayed', 'pushTasksTypesId'], 'required'],
            [['dateDelayed', 'dateCreate', 'image'], 'safe'],
            [['pushTasksStatusesId', 'pushTasksAudiencesId', 'pushTasksTypesId'], 'integer'],
            ['gender', 'genderRequired'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок уведомления',
            'text' => 'Текст уведомления',
            'image' => 'Картинка',
            'dateDelayed' => 'Планируемая дата отправки',
            'dateCreate' => 'Дата создания',
            'pushTasksStatusesId' => 'Статус',
            'pushTasksAudiencesId' => 'Аудитория',
            'pushTasksTypesId' => 'Тип пуш уведомления',
            'gender' => 'Пол',
            'sectionId' => 'Раздел каталога',
            'collectionId' => 'Подборка',
            'brandId' => 'Бренд',
            'productId' => 'ID или артикул товара',
            'productName' => 'Название товара',
            'usersId' => 'Id пользователей',
        ];
    }

    public function getPushTasksAudience(): ActiveQuery
    {
        return $this->hasOne(TaskAudience::className(), ['id' => 'pushTasksAudiencesId']);
    }

    public function getPushTasksStatus(): ActiveQuery
    {
        return $this->hasOne(TaskStatus::className(), ['id' => 'pushTasksStatusesId']);
    }

    public function getPushTasksType(): ActiveQuery
    {
        return $this->hasOne(TaskType::className(), ['id' => 'pushTasksTypesId']);
    }
}